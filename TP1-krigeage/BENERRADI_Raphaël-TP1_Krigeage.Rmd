---
title: "TP1 Geostatistiques Krigeage"
subtitle: "M2 MSV 2023/2024"
author: "Raphaël BENERRADI"
# date: "2024-02-25"
date: "`r format(Sys.Date(), '%e %b %Y')`"
output:
  html_document:
    theme: readable
    toc: yes
    number_sections: yes
    toc_float: yes
    highlight: tango
    df_print: paged
    code_folding: show
  pdf_document:
    toc: no
encoding: UTF-8
editor_options: 
  markdown: 
    wrap: 72
---

```{r setup, include=FALSE}
fmt <- rmarkdown::default_output_format(knitr::current_input())$name
is_html = (fmt == "html_document")

knitr::opts_chunk$set(include=is_html,
                      echo = TRUE, # Chunks visibles
                      eval = TRUE, # Chunks évalués
                      message = FALSE, # Messages R non affichés
                      warning = FALSE,  # Avertissement R non affichés
                      # results="hold", # Résultats en un bloc
                      class.source = "fold-hide") # Chucks cachés par défaut
```


<span style="display: block; font-size: 2em; margin-top: 0.67em; margin-bottom: 0.67em; margin-left: 0; margin-right: 0; font-weight: bold; font-family: 'Raleway'">
Etude de cas : pollution de l'air </span>

<div style="color:#AAAAAA ; font-style: italic; font-family: 'Raleway'">

**Enoncé :**

On souhaite réaliser une carte quotidienne de concentration d'ozone sur la 
région Parisienne. Pour cela on dispose chaque jour des sorties d'un modèle 
déterministe mis au point au Laboratoire de Météorologie Dynamique (Ecole 
Polytechnique) et des mesures de concentration d'ozone effectuées par AirParif 
en 21 stations.

Le fichier [stationsKm.txt](stationsKm.txt) contient un tableau formé des 
colonnes suivantes :

- colonne 1 : abscisses (en km) des stations

- colonne 2 : ordonnées (en km) des stations

- colonne 3 : mesures aux stations (en $\mu g/m^3$)

- colonne 4 : valeur du modèle aux stations


Le fichier [grilleKm.txt](grilleKm.txt) contient : 

- colonne 1 : abscisses (en km) des points de grille

- colonne 2 : ordonnées (en km) des points de grille

- colonne 3 : valeur du modèle des points de grille

**On admet la présence d'un bruit de mesure de $10\mu g/m^3$ sur les observations.**

</div>


On va d'abord importer les librairies et les fichiers. 


```{r libraries}
library(geoR)

library(ggplot2)
library(viridis)

library(tidyverse)
library(knitr)
```

```{r import_files}
set.seed(1234)

stationsKm = read.table("stationsKm.txt",header=TRUE)

kable(head(stationsKm), caption = "Fichier stationsKm.txt (premières lignes)")
kable(summary(stationsKm), caption = "Résumé de stationsKm.txt")
# summary(stationsKm)
cat("Dimentions de stationsKm.txt : ", paste0(dim(stationsKm),  collapse=" ; "))

grilleKm = read.table("grilleKm.txt",header=TRUE)
kable(head(grilleKm), caption = "Fichier grilleKm.txt (premières lignes)")
kable(summary(grilleKm), caption = "Résumé de grilleKm.txt")
# summary(grilleKm)
cat("Dimentions de grilleKm.txt : ", paste0(dim(grilleKm),  collapse=" ; "))

x_lim_data = c(min(min(stationsKm$x), min(grilleKm$x))-5,
               max(max(stationsKm$x), max(grilleKm$x))+5 )
y_lim_data = c(min(min(stationsKm$y), min(grilleKm$y))-5,
               max(max(stationsKm$y), max(grilleKm$y))+5 )
```



# Kriegeage sur les 21 stations

<div style="color:#AAAAAA ; font-style: italic; font-family: 'Raleway'">
Faire une carte en estimant la concentration en chaque point de la grille par 
krigeage à partir des mesures aux 21 stations. 
</div>

On commence par visualiser les données. 

```{r visualisation_stations}
# plot des stations
geo_stationsKm_mes = as.geodata(stationsKm[, c("x", "y", "z")])
par(oma = c(0, 0, 3, 0))  # Adjust the top margin (third value in oma)
plot(geo_stationsKm_mes)
mtext("Geodata stationsKm", outer=TRUE, line=1.4, font=2, cex=1.4)

ggplot(stationsKm) + 
  geom_point(aes(x = x, y = y, color = z), size = 5) + 
  scale_color_viridis_c(option = "plasma") + 
  labs(x = "X-axis", y = "Y-axis", color = "Concentration") +
  theme_minimal() + 
  ggtitle("Concentration en ozone (µg/m3) aux stations") +
  theme(plot.title = element_text(hjust = 0.5)) +
  theme(legend.key.height = unit(2, "cm"))

```

On remarque d'abord que les données de concentration ne sont pas constantes sur
la zone géographique. Il semble bien y avoir un effet spatial. On remarque par 
exemple que les relevés des stations au Nord-Ouest sont bien supérieurs à ceux des
autres stations. Les données sont principalement autour des 130 $\mu g/m^3$, 
mais s'étendent entre 100 et 220 $\mu g/m^3$.

Pour étudier cet effet spatial, on peut estimer les variogrammes et ajuster les 
modèles. Tout d'abord on affiche la nuée variographique.

```{r variogrammes_nuee, fig.width=9, fig.height=7}
# Variogrammes (nuée)
vario.nuee = variog(geo_stationsKm_mes, op = "cloud", messages=FALSE)
par(oma = c(0, 0, 0, 0))
plot(vario.nuee, main = "Nuée variographique (stationsKm)",pch='+')
```

Puis le variogramme empirique. On choisit 20 classes pour regrouper les points 
par classes de distance. 2 classes ne sont pas représentées car elles ne 
contiennent pas de point.
On fait le choix de 20 classes par tatonnement pour avoir un variogramme assez 
lisse et assez de points pour ajuster un variogramme par la suite. 

```{r variogrammes_empiriques, fig.width=9, fig.height=7}
# Variogramme (empirique)
n_bins = 20
vario.emp = variog(geo_stationsKm_mes, max.dist = vario.nuee$max.dist, 
                   breaks=seq(0, vario.nuee$max.dist, length.out=n_bins+1), 
                   messages=FALSE)
plot(vario.emp, main = "Variogramme empirique (stationsKm)")
mtext(paste0(n_bins, " classes demandées, ", length(vario.emp$u), " complétées"),
      outer=TRUE, line=-3.8, font=3, cex=0.9)
grid(lty = 2, col = "gray")
```


Le variogramme semble plutôt convexe sur les premières classes de distance. 

On va chercher des types de variogrammes pouvant être convexes :

- Gaussien : $\gamma(h) = \sigma^2 \left(1 - \exp \left[ -\left(\frac{h}{\phi}\right)^2 \right]\right) + \tau$

- Puissance : $\gamma(h) = \sigma^2 h^\phi + \tau$

- Cauchy : $\gamma(h) = \sigma^2 \left[1 - \left(1 + \left(\frac{h}{\phi}\right)^2 \right)^{-\kappa} \right] + \tau \quad \quad \text{avec} \quad \kappa = 0.5 \text{ par défaut}$

- Cubique : $\gamma(h) = \begin{cases} \sigma^2 \left[7\left(\frac{h}{\phi}\right)^2 - 8.75\left(\frac{h}{\phi}\right)^3 + 3.5\left(\frac{h}{\phi}\right)^5 - 0.75\left(\frac{h}{\phi}\right)^7 \right] + \tau, & \text{si } h < \phi \\ \sigma^2 + \tau,    & \text{sinon} \end{cases}$

On vérifie que ces types de variogrammes peuvent bien être convexes avec le
graphe suivant. 

```{r check_concave_variogrammes}
echelle = 10000
portee = 100
nugget = 10
puissance = 1.5
sigma_puis = 1.2


# sigma2_1 = vario.adj.gaus$cov.pars[1]
# phi_1 = vario.adj.gaus$cov.pars[2]
sigma2_1 = echelle
phi_1 = portee
curve(sigma2_1 * (1 - exp(-(x/phi_1)^2)) + nugget, 0, 100, col=2, lty=2, ylim=c(0, 5000),
      main=paste0("Tracés manuels pour les types de variogrammes ajustés\n",
                  "Portée = ", portee, " ; Echelle ~ ", echelle, " ; Nugget = ", nugget,
                  "\nPour puissance : puissance = ", puissance, " ; sigma2 = ", sigma_puis), 
      xlab="Distance", ylab="Covariance")

# sigma2_2 = vario.adj.pow$cov.pars[1]
# phi_2 = vario.adj.pow$cov.pars[2]
sigma2_2 = sigma_puis
phi_2 = puissance
curve(sigma2_2 * x^phi_2 + nugget, 0, 100, col=3, lty=2, add=TRUE)

# sigma2_3 = vario.adj.cau$cov.pars[1]
# phi_3 = vario.adj.cau$cov.pars[2]
sigma2_3 = echelle
phi_3 = portee
kappa_3 = 0.5
curve(sigma2_3 * (1 - (1 + (x/phi_3)^2)^(-kappa_3)) + nugget, 0, 100, col=4, lty=2, add=TRUE)

# sigma2_4 = vario.adj.cub$cov.pars[1]
# phi_4 = vario.adj.cub$cov.pars[2]
sigma2_4 = echelle
phi_4 = portee
curve(sigma2_4 * (7*(x/phi_4)^2 - 8.75*(x/phi_4)^3 + 3.5*(x/phi_4)^5 - 0.75*(x/phi_4)^7) + nugget, 0, 100, col=5, lty=2, add=TRUE)

legend("topleft", legend=c("Gaussien", "Puissance", "Cauchy", "Cubique"), col=2:5, lty=1)

```

Ajustons maintenant ces variogrammes au variogramme empirique. On prendra une 
pépite de 10 $\mu g/m^3$ pour représenter le bruit de mesure sur les observations. 

```{r variogrammes_ajuste_bons, fig.width=9, fig.height=7}
nugget = 10


plot(vario.emp, main = "Variogramme empirique (stationsKm)")
grid(lty = 2, col = "gray")
# Variogrammes ajustés (qui fonctionnent)
vario.adj.gaus = variofit(vario.emp, cov.model="gaussian", fix.nugget=TRUE, 
                          nugget=nugget, ini.cov.pars= c(1, 1), messages=FALSE)
lines(vario.adj.gaus, col=2)
vario.adj.pow  = variofit(vario.emp, cov.model="power", fix.nugget=TRUE, 
                          nugget=nugget, ini.cov.pars= c(1, 1), messages=FALSE)
lines(vario.adj.pow, col=3)
vario.adj.cau  = variofit(vario.emp, cov.model="cauchy", fix.nugget=TRUE, 
                          nugget=nugget, ini.cov.pars= c(1, 1), messages=FALSE)
lines(vario.adj.cau, col=4)
vario.adj.cub  = variofit(vario.emp, cov.model="cubic", fix.nugget=TRUE, 
                          nugget=nugget, ini.cov.pars= c(1, 10), messages=FALSE)
lines(vario.adj.cub, col=5)
legend("topleft", legend=c("Gaussien", "Puissance", "Cauchy", "Cubique"), col=2:5, lty=1)
```


D'autres types de variogrammes ne s'ajustent pas bien car ils sont 
nécessairement concaves : 

- Circulaire : $\theta = \min(\frac{h}{\phi}, 1) \quad \text{et} \quad g(h) = 2\frac{ \left( \theta\sqrt{1-\theta^2} + \sin^{-1}(\sqrt{\theta}) \right) } {\pi}  \\  \gamma(h) = \begin{cases} \sigma^2 g(h) + \tau, & \text{si } h < \phi \\ \sigma^2 + \tau, & \text{sinon} \end{cases}$

- Matern : $\gamma(h) = \sigma^2 \left[ 1 - \frac{1}{2^{\nu-1}\Gamma(\kappa)} \left( \frac{\sqrt{2\kappa}h}{\phi} \right)^\kappa K_\kappa\left( \frac{\sqrt{2\kappa}h}{\phi} \right) \right] + \tau \quad \kappa = 0.5 \text{ par défaut}$

- Sphérique : $\gamma(h) = \begin{cases} \sigma^2 \left[ 1.5\frac{h}{\phi} - 0.5\left(\frac{h}{\phi}\right)^3 \right] + \tau, & \text{si } h < \phi \\ \sigma^2 + \tau, & \text{sinon} \end{cases}$

- Exponentiel : $\gamma(h) = \sigma^2 \left[ 1 - \exp \left( -\frac{h}{\phi} \right) \right] + \tau$

On va d'abord tracer ces variogrammes avec des paramètres arbitraires pour se 
donner une idée de l'allure concave de ces variogrammes. 

```{r check_concave_variogrammes_mauvais}
echelle = 10000
portee = 500
nugget = 10

# sigma2_6 = vario.adj.cir$cov.pars[1]
# phi_6 = vario.adj.cir$cov.pars[2]
sigma2_6 = echelle
phi_6 = portee
circ = function(x) {
  theta = pmin(1, x/phi_6)
  g = 2 * (theta * sqrt(1-theta^2) + asin(sqrt(theta))) / pi
  sigma2_6 * g + nugget
}
curve(circ, 0, 100, col=6, lty=2, ylim=c(0, 5000),
      main=paste0("Tracés manuels pour les types de variogrammes ajustés\n",
                  "Portée = ", portee, " ; Echelle ~ ", echelle, " ; Nugget = ", nugget), 
      xlab="Distance", ylab="Covariance")

# sigma2_7 = vario.adj.mat$cov.pars[1]
# phi_7 = vario.adj.mat$cov.pars[2]
sigma2_7 = echelle
phi_7 = portee
kappa_7 = 0.5
curve(sigma2_7 * (1 - 1/(2^(kappa_7-1)*gamma(kappa_7)) * (sqrt(2*kappa_7)*x/phi_7)^kappa_7 * besselK(sqrt(2*kappa_7)*x/phi_7, kappa_7)) + nugget, 0, 100, col=7, lty=2, add=TRUE)

# sigma2_8 = vario.adj.sph$cov.pars[1]
# phi_8 = vario.adj.sph$cov.pars[2]
sigma2_8 = echelle
phi_8 = portee
curve(sigma2_8 * (1.5*x/phi_8 - 0.5*(x/phi_8)^3) + nugget, 0, 100, col=8, lty=2, add=TRUE)

# sigma2_9 = vario.adj.exp$cov.pars[1]
# phi_9 = vario.adj.exp$cov.pars[2]
sigma2_9 = echelle
phi_9 = portee
curve(sigma2_9 * (1 - exp(-x/phi_9)) + nugget, 0, 100, col=9, lty=2, add=TRUE)

legend("topleft", legend=c("Circulaire", "Matern", "Sphérique", "Exponentiel"), col=6:9, lty=1)
```

On peut essayer d'ajuster (en vain) ces variogrammes au variogramme empirique.

```{r variogrammes_ajuste_mauvais, fig.width=9, fig.height=7}
plot(vario.emp, main = "Variogramme empirique (stationsKm)")
grid(lty = 2, col = "gray")
# Variogrammes ajustés (qui ne fonctionnent pas)
vario.adj.cir  = variofit(vario.emp, cov.model="circular", fix.nugget=TRUE, 
                          nugget=nugget, ini.cov.pars= c(1, 5), messages=FALSE)
lines(vario.adj.cir, col=6)
vario.adj.mat  = variofit(vario.emp, cov.model="matern", fix.nugget=TRUE, 
                          nugget=nugget, ini.cov.pars= c(1, 100), messages=FALSE)
lines(vario.adj.mat, col=7, lty=4)
vario.adj.sph  = variofit(vario.emp, cov.model="spherical", fix.nugget=TRUE, 
                          nugget=nugget, ini.cov.pars= c(1, 100), messages=FALSE)
lines(vario.adj.sph, col=8, lty=4)
vario.adj.exp  = variofit(vario.emp, cov.model="exponential", fix.nugget=TRUE, 
                          nugget=nugget, ini.cov.pars= c(1000, 100), messages=FALSE)
lines(vario.adj.exp, col=9, lty=4)
legend("topleft", legend=c("Circular", "Matern", "Spherical", "Exponential"), col=6:9, lty=1)
```



Parmi les 4 variogrammes qui fonctionnaient bien, on va prendre arbitrairement 
le variogramme gaussien. Ce type de variogramme a une expression relativement 
simple, il est régulier, et il atteint son pallier à l'infini (valeur finie pour 
les grandes distances). Ce sont des propriétés intéressantes pour une variogramme. 

A partir de ce variogramme, on peut réaliser le krigeage. 
Pour cela, on récupère la grille de `grilleKm`, et on réalise un krigeage 
ordinaire.

```{r krigeage}
vario.adj = vario.adj.gaus

# Krigeage
grille_xy = grilleKm[, c("x", "y")]
Kcontrol = krige.control(type.krige = "OK", obj.model = vario.adj)
K = krige.conv(geo_stationsKm_mes, loc = grille_xy, krige = Kcontrol, 
               output = list("messages" = FALSE))
# str(K)


# # Plot du krigeage
color_stations = "grey70"
# image(K, col=terrain.colors(64), coords.data=geo_stationsKm_mes$coords,
#       xlim=x_lim_data, ylim=y_lim_data, zlim=c(min(K$predict), max(K$predict)),
#       x.leg=c(20,120), y.leg=c(-12,-5))
# title(main="Krigeage")
# 
# image(K, col=terrain.colors(64), coords.data=geo_stationsKm_mes$coords,
#       xlim=x_lim_data, ylim=y_lim_data, zlim=c(min(K$krige.var), max(K$krige.var)),
#       x.leg=c(20,120), y.leg=c(-12,-5), val=K$krige.var)
# title(main="Variance de krigeage  ")

Kres = data.frame(x=grille_xy[,1], y=grille_xy[,2], 
                  pred=K$predict, var=K$krige.var)
# Prediction
ggplot(Kres, aes(x = x, y = y)) + 
  geom_raster(aes(fill = pred)) + 
  scale_fill_viridis_c(option = "plasma") +  # Change color scale
  geom_point(data = stationsKm, aes(x = x, y = y), color = color_stations, size = 2.5) +  # Add station points
  labs(x = "X-axis", y = "Y-axis", fill = "Prediction") +  # Add axis labels
  theme_minimal() +  # Use a minimal theme
  ggtitle("Krigeage\nprédiction de la concentration en ozone (µg/m3)") +
  theme(plot.title = element_text(hjust = 0.5)) +  # Center plot title
  theme(legend.key.height = unit(2, "cm"))
# Variance
ggplot(Kres, aes(x = x, y = y)) + 
  geom_raster(aes(fill = var)) + 
  scale_fill_viridis_c(option = "magma") +  # Change color scale
  geom_point(data = stationsKm, aes(x = x, y = y), color = color_stations, size = 2.5) +  # Add station points
  labs(x = "X-axis", y = "Y-axis", fill = "Variance") +  # Add axis labels
  theme_minimal() +  # Use a minimal theme
  ggtitle("Variance de krigeage\nsur la concentration en ozone prédite en (µg/m3)²") +
  theme(plot.title = element_text(hjust = 0.5)) +  # Center plot title
  theme(legend.key.height = unit(2, "cm"))
```


# Prédiction par le modèle

<div style="color:#AAAAAA ; font-style: italic; font-family: 'Raleway'">
Tracer la carte des concentrations données par le modèle. 
Comparer avec la carte précédente.
</div>

Les concentrations données par le modèle sont dans `grilleKm`. 

```{r model}
# Prediction
ggplot(grilleKm, aes(x = x, y = y)) + 
  geom_raster(aes(fill = z), interpolate=TRUE) + 
  scale_fill_viridis_c(option = "plasma") +
  geom_point(data = stationsKm, aes(x = x, y = y), color = color_stations, size = 2.5) +
  labs(x = "X-axis", y = "Y-axis", fill = "Prediction") +  # Add axis labels
  theme_minimal() +
  ggtitle("Modèle\ni.e. simulation de la concentration en ozone (µg/m3)") +
  theme(plot.title = element_text(hjust = 0.5)) +  # Center plot title
  theme(legend.key.height = unit(2, "cm"))
```

Tout comme pour les données des stations, on remarque une zone avec une 
concentration particulièrement haute : la partie Nord-Ouest.

Regardons la différence entre les deux cartes. 

```{r diff}
# Différence
ggplot(Kres, aes(x = x, y = y)) + 
  geom_raster(aes(fill = pred - grilleKm$z)) + 
  scale_fill_viridis_c() +
  geom_point(data = stationsKm, aes(x = x, y = y), color = color_stations, size = 2.5) +
  labs(x = "X-axis", y = "Y-axis", fill = "Prediction") +  # Add axis labels
  theme_minimal() +
  ggtitle("Différence\n= prédiction par krigeage - modèle") +
  theme(plot.title = element_text(hjust = 0.5)) +  # Center plot title
  theme(legend.key.height = unit(2, "cm"))
```

On retrouve une différence de l'ordre de $0 \text{ à } 50 \mu g/m^3$ entre le 
krigeage et le modèle. C'est une différence non négligeable pour des 
concentrations prédites entre 100 et 200 $\mu g/m^3$. 
Le krigeage donne en général une valeur plus grande que le modèle. 
Il peut être intéressant de corriger le modèle pour éviter qu'il sous-estime 
les concentrations d'ozone. 


# Combinaison des deux approches

<div style="color:#AAAAAA ; font-style: italic; font-family: 'Raleway'">
On désire combiner les 2 approches. Pour cela on corrige le modèle déterministe en chaque point de la grille par une estimation de la différence concentration-modèle obtenue en krigeant les différences observation-modèle aux stations.     

Comparer aux deux cartes précédentes. Commenter.
</div>

Regardons d'abord les différences entre l'observation et le modèle pour les 
stations de mesure. 

```{r compute_diff}
# Calcul de la différence
stationsKm$diff = stationsKm$z - stationsKm$mod
geo_stationsKm_diff = as.geodata(stationsKm[, c("x", "y", "diff")])
par(oma = c(0, 0, 3, 0))  # Adjust the top margin (third value in oma)
plot(geo_stationsKm_diff)
mtext("Geodata stationsKm (différence  = observation - modèle)", outer=TRUE, line=1.4, font=2, cex=1.4)

ggplot(stationsKm) + 
  geom_point(aes(x = x, y = y, color = diff), size = 5) +
  scale_color_viridis_c(option = "plasma") + 
  labs(x = "X-axis", y = "Y-axis", color = "Concentration") +
  theme_minimal() + 
  ggtitle("Différence concentration en ozone (µg/m3)\nobservation - modèle aux stations") +
  theme(plot.title = element_text(hjust = 0.5)) +
  theme(legend.key.height = unit(2, "cm"))

cat("Résumé de la différence (observation - modèle) : \n")
summary(stationsKm$diff)

```

C'est principalement dans le centre de la région que le modèle sous-estime la 
concentration en ozone.


On va estimer les variogrammes sur cette différence. 
Dans un premier temps, on affiche la nuée variographique, et le variogramme 
empirique. On choisit 18 classes pour regrouper les points par classes de 
distance, pour des raisons similaires à celles évoquées précédemment (variogramme
lisse et nombre de points suffisant). 

```{r variogrammes_nuee_diff, fig.width=9, fig.height=7}
# Variogrammes (nuée)
vario.diff.nuee = variog(geo_stationsKm_diff, op = "cloud", messages=FALSE)
par(oma = c(0, 0, 0, 0))
plot(vario.diff.nuee, main = "Nuée variographique : différence sur les stations (observation - modèle)",pch='+')

# Variogramme (empirique)
n_bins = 18
vario.diff.emp = variog(geo_stationsKm_diff, max.dist = vario.diff.nuee$max.dist, 
                        breaks=seq(0, vario.diff.nuee$max.dist, length.out=n_bins+1), 
                        messages=FALSE)
plot(vario.diff.emp, main = "Variogramme empirique : différence sur les stations (observation - modèle)")
mtext(paste0(n_bins, " classes demandées, ", length(vario.emp$u), " complétées"),
      outer=TRUE, line=-3.8, font=3, cex=0.9)
grid(lty = 2, col = "gray")
```

Puis on ajuste les variogrammes. On prend une pépite de 10 pour représenter le 
bruit de mesure. On considère le modèle comme étant précis (au sens "pas de 
variabilité dans sa prédiction") donc ce dernier n'ajoute pas de bruit à la 
différence qu'on analyse. 

On va choisir un variogramme exponentiel. C'est un variogramme assez répandu 
et il semble adapté à l'allure du variogramme empirique. 

```{r variogrammes_ajuste_diff, fig.width=9, fig.height=7}
# Variogrammes ajustés
nugget = 10

plot(vario.diff.emp, main = paste0("Variogramme empirique : différence sur les stations (observation - modèle)",
                              "\navec variogramme ajusté (exponentiel)") )
grid(lty = 2, col = "gray")
vario.diff.adj.exp = variofit(vario.diff.emp, cov.model="exp", fix.nugget=TRUE, 
                              nugget=nugget, ini.cov.pars= c(1, 1), messages=FALSE)
lines(vario.diff.adj.exp, col=2)
```

On peut maintenant réaliser le krigeage de la différence. 

```{r krigeage_diff}
# Krigeage
Kcontrol.diff = krige.control(type.krige = "OK", obj.model = vario.diff.adj.exp)
K.diff = krige.conv(geo_stationsKm_diff, loc = grille_xy, krige = Kcontrol.diff, 
                    output = list("messages" = FALSE))
# str(K.diff)

Kres.diff = data.frame(x=grille_xy[,1], y=grille_xy[,2], 
                       pred=K.diff$predict, var=K.diff$krige.var)
# Prediction
ggplot(Kres.diff, aes(x = x, y = y)) + 
  geom_raster(aes(fill = pred)) + 
  scale_fill_viridis_c() +
  geom_point(data = stationsKm, aes(x = x, y = y), color = color_stations, size = 2.5) +
  labs(x = "X-axis", y = "Y-axis", fill = "Prediction") +
  theme_minimal() +
  ggtitle("Krigeage\nprédiction de la différence concentration en ozone (µg/m3)\nobservation - modèle") +
  theme(plot.title = element_text(hjust = 0.5)) +  # Center plot title
  theme(legend.key.height = unit(2, "cm"))

# Variance
ggplot(Kres.diff, aes(x = x, y = y)) + 
  geom_raster(aes(fill = var)) + 
  scale_fill_viridis_c(option = "magma") + 
  geom_point(data = stationsKm, aes(x = x, y = y), color = color_stations, size = 2.5) + 
  labs(x = "X-axis", y = "Y-axis", fill = "Variance") +
  theme_minimal() +
  ggtitle("Variance de krigeage\nsur la différence observation - modèle en (µg/m3)²") +
  theme(plot.title = element_text(hjust = 0.5)) +  # Center plot title
  theme(legend.key.height = unit(2, "cm"))

```

On a alors une estimation de la différence entre l'observation et le modèle sur 
la région considérée. 

On peut maintenant corriger le modèle par la prédiction de la différence : on 
ajoute la prédiction de la différence (observation - modèle) à la concentration 
estimée par le modèle. 

La variance associée est celle obtenue lors du krigeage de la différence. 

```{r correction}
grilleKm$correc = grilleKm$z + Kres.diff$pred
ggplot(grilleKm, aes(x = x, y = y)) + 
  geom_raster(aes(fill = correc), interpolate=TRUE) + 
  scale_fill_viridis_c(option = "plasma") + 
  geom_point(data = stationsKm, aes(x = x, y = y), color = color_stations, size = 2.5) +
  labs(x = "X-axis", y = "Y-axis", fill = "Prediction") +
  theme_minimal() +
  ggtitle("Modèle corrigé de la différence à l'observation\nConcentration en ozone (µg/m3)") +
  theme(plot.title = element_text(hjust = 0.5)) +  # Center plot title
  theme(legend.key.height = unit(2, "cm"))
```

Comparons les 3 méthodes de prédiction : krigeage, modèle, et modèle corrigé.

```{r comparison, fig.width=12, fig.height=6}
estim_kirg = Kres %>% select(x, y, pred) %>% mutate(method = "krigeage") %>% rename(value = pred)
estim_model = grilleKm %>% select(x, y, z) %>% mutate(method = "model") %>% rename(value = z)
estim_corr = grilleKm %>% select(x, y, correc) %>% mutate(method = "model_corr") %>% rename(value = correc)
estim = rbind(estim_kirg, estim_model, estim_corr)
estim$method = factor(estim$method, levels = c("krigeage", "model", "model_corr"),
                      labels = c("Krigeage", "Modèle", "Modèle corrigé"))

# Facet plot
ggplot(estim, aes(x = x, y = y)) + 
  geom_raster(aes(fill = value)) + 
  scale_fill_viridis_c(option = "plasma") + 
  geom_point(data = stationsKm, aes(x = x, y = y), color = color_stations, size = 2.5) +
  facet_wrap(~method, ncol = 3) +
  labs(x = "X-axis", y = "Y-axis", fill = "Concentration\nen ozone (µg/m3)") +
  theme_minimal() +
  ggtitle("Comparaison des méthodes de prédiction") +
  theme(plot.title = element_text(hjust = 0.5)) +
  theme(legend.key.width = unit(3, "cm")) + 
  theme(legend.position = "bottom")
```


Le krigeage sur les stations donnait une prédiction très grossière. 
Pour coller au mieux aux observations, cette méthode a eu 
tendance à estimer des concentrations élevées dans la partie Ouest/Nord-Ouest 
de la région. Le modèle quant à lui avait tendance à sous-estimer les 
concentrations. 
En combinant les deux approches (krigeage de la différence et correction du 
modèle), on a pu utiliser l'information de l'observation pour corriger le modèle.
La correction a plutôt tendance à augmenter la concentration estimée, en 
particulier dans la partie centrale.  



# Evaluation des seuils de risque

Différents seuils existent pour évaluer le risque lié à la concentration en 
ozone. On peut citer les seuils suivants :
un seuil d'information et de recommandation lorsque la concentration en ozone 
dépasse les 180 $\mu g/m^3$, et un seuil d'alerte à 240 $\mu g/m^3$.

*(Voir le [site du Ministère de la Transition écologique et de la cohésion des territoires](https://www.ecologie.gouv.fr/pollution-lozone-mesures-mises-en-oeuvre))*

On peut alors évaluer les zones prédites par notre modèle corrigé, sur lesquelles 
ces seuils sont dépassés. 


```{r seuils}
seuil_info = 180
seuil_alerte = 240

estim_corr$risque = ifelse(grilleKm$correc > seuil_alerte, "alerte",
                           ifelse(grilleKm$correc > seuil_info, "recommandation", "normal"))

ggplot(estim_corr, aes(x = x, y = y)) +
  geom_raster(aes(fill = risque), interpolate=TRUE) +
  scale_fill_manual(values = c("normal" = "palegreen2", "recommandation" = "darkorange", "alerte" = "red3")) +
  geom_point(data = stationsKm, aes(x = x, y = y), color = color_stations, size = 2.5) +
  labs(x = "X-axis", y = "Y-axis", fill = "Risque") +
  theme_minimal() +
  ggtitle("Risque lié à la concentration en ozone\nselon les seuils d'information et d'alerte") +
  theme(plot.title = element_text(hjust = 0.5)) +
  theme(legend.key.height = unit(2, "cm"))
```


On peut même estimer la probabilité de dépassement du seuil de recommandations 
en chaque point a partir de la valeur estimée et de la variance de krigeage. 
En effet, si on suppose que la concentration suit une loi normale, on peut utiliser 
la prédiction de la valeur et de la variance pour estimer la probabilité de 
dépassement du seuil. 

```{r proba}
estim_corr$proba = 1 - pnorm((seuil_info - estim_corr$value) / sqrt(Kres.diff$var))

ggplot(estim_corr, aes(x = x, y = y)) +
  geom_raster(aes(fill = proba), interpolate=TRUE) +
  scale_fill_gradientn(colors = c("#78c2ad", "#ffeb84", "#f16c5a"), name = "Probabilité") +
  geom_point(data = stationsKm, aes(x = x, y = y), color = color_stations, size = 2.5) +
  labs(x = "X-axis", y = "Y-axis", fill = "Probabilité") +
  theme_minimal() +
  ggtitle("Probabilité de dépassement du seuil de recommandation\nen fonction de la concentration en ozone\n(avec hypothèse de normalité)") +
  theme(plot.title = element_text(hjust = 0.5)) +
  theme(legend.key.height = unit(2, "cm"))
```


Sans surprise, on remarque encore que la patie Nord-Ouest est particulièrement 
touchée et à risque. 

